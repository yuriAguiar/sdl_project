//#define SDL_MAIN_HANDLED // Use if you want to manually provide an entry point
#include <iostream>
#include <Windows.h>

#include "MainGame.h"

int main(int argc, char* args[])
{
	MainGame mainGame;
	mainGame.Run();

	return 0;
}
