#pragma once

#include <SDL.h>

class Square
{
public:
	Square(int originX, int originY, int width, int height);
	~Square();

	const SDL_Rect& GetDimensions();
	void MoveRandom();

	SDL_Rect _dimensions;
};
