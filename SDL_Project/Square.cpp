#include "Square.h"
#include <iostream>

Square::Square(int originX, int originY, int width, int height)
{
	_dimensions.x = originX;
	_dimensions.y = originY;
	_dimensions.w = width;
	_dimensions.h = height;
}

Square::~Square()
{
}

const SDL_Rect& Square::GetDimensions()
{
	return _dimensions;
}

void Square::MoveRandom()
{
	_dimensions.x += 1;
}
