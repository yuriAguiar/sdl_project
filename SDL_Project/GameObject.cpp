#include "GameObject.h"

GameObject::GameObject(SDL_Rect Square, SDL_Renderer* Renderer, int xPos, int yPos)
	: _square(Square), _renderer(Renderer), _xPos(xPos), _yPos(yPos)
{
}

GameObject::~GameObject()
{
}

void GameObject::Update()
{
	_xPos++;
	_yPos++;

	_square.x = _xPos;
	_square.y = _yPos;

}

void GameObject::Render()
{
	SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(_renderer, &_square);
}
