#pragma once

#include "MainGame.h"

class GameObject
{
public:
	GameObject() = default;
	GameObject(SDL_Rect Square, SDL_Renderer* Renderer, int xPos, int yPos);
	~GameObject();

	void Update();
	void Render();

private:
	int _xPos = 0;
	int _yPos = 0;

	SDL_Rect _square;
	SDL_Renderer* _renderer = nullptr;
};