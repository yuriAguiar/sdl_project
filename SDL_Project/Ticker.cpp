#include "Ticker.h"

void Ticker::CapFps()
{
	if (FRAME_DELAY > _lastFrameTime)
	{
		SDL_Delay(FRAME_DELAY - _lastFrameTime);
	}
}

int Ticker::CalculateLastFrameTime()
{
	_lastFrameTime = SDL_GetTicks() - _time;

	return _lastFrameTime;
}
