#include "MainGame.h"
#include "Square.h"
#include "GameObject.h"
#include "Ticker.h"

#include <iostream>
#include <string>
#include <SDL_ttf.h>

void FatalError(std::string errorString)
{
	std::cout << errorString << std::endl;
	std::cout << "Enter any key to quit." << std::endl;

	int tmp;
	std::cin >> tmp;
	TTF_Quit();
	SDL_Quit();
}

MainGame::MainGame()
{
	_window = nullptr;
	_renderer = nullptr;
	_textDimensions = { 0, 0, 0, 0 };
	_squaresNum = 0;
	_screenWidth = 2048;
	_screenHeight = 768;
	_gameState = GameState::PLAY;
}

MainGame::~MainGame()
{
}

void MainGame::Run()
{
	InitSystems();

	SDL_Color textColor = { 255, 255, 255, 255 };
	SDL_Surface* _surface = TTF_RenderText_Solid(_font, _text.c_str(), textColor);
	_texture = SDL_CreateTextureFromSurface(_renderer, _surface);

	int texW = 0;
	int texH = 0;
	SDL_QueryTexture(_texture, NULL, NULL, &texW, &texH);
	_textDimensions = { 0, 0, texW, texH };

	SDL_Rect square = { 100, 100, 100, 100 };
	_gameObject = new GameObject(square, _renderer, 100, 100);

	SDL_StartTextInput();
	GameLoop();
	SDL_StopTextInput();

	SDL_DestroyWindow(_window);
	SDL_DestroyRenderer(_renderer);
	TTF_CloseFont(_font);
	TTF_Quit();
	SDL_Quit();
}

void MainGame::InitSystems()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	TTF_Init();

	_window = SDL_CreateWindow(
		"Game Engine",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		_screenWidth,
		_screenHeight,
		SDL_WINDOW_OPENGL && SDL_GL_DOUBLEBUFFER);
	SDL_SetWindowFullscreen(_window, 0);

	if (_window == nullptr)
	{
		FatalError("SDL Window could not be created");
	}

	GLenum glewInitCode =  glewInit();

	/*if (glewInitCode != GLEW_OK)
	{
		FatalError("Could not initialize Glew");
	}*/

	_renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED && SDL_HINT_RENDER_VSYNC);
	_font = TTF_OpenFont("/Projects/SDL_Project/Debug/arial.ttf", 25);
	InitializeSquares();
}

void MainGame::GameLoop()
{
	while (_gameState != GameState::EXIT)
	{
		Ticker dt(SDL_GetTicks());

		ProcessInput();
		Update();
		Draw();

		dt.CalculateLastFrameTime();
		dt.CapFps();
	}
}

void MainGame::ProcessInput()
{
	SDL_Event sdlEvent;

	while (SDL_PollEvent(&sdlEvent))
	{
		switch (sdlEvent.type)
		{
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			// Log mouse cursor XY position
			//std::cout << sdlEvent.motion.x << " " << sdlEvent.motion.y << std::endl;
			break;
		default:
			break;
		}

		if (sdlEvent.type == SDL_TEXTINPUT)
		{
			if (sdlEvent.type == SDL_KEYDOWN && sdlEvent.key.keysym.sym == SDLK_BACKSPACE && _text.length() > 0)
			{
				_text = _text.substr(0, _text.length() - 1);
			}
			else if (sdlEvent.type == SDL_TEXTINPUT)
			{
				_text += sdlEvent.text.text;
			}
		}

		if (sdlEvent.type == SDL_MOUSEBUTTONDOWN)
		{
			switch (sdlEvent.button.button)
			{
			case SDL_BUTTON_LEFT:
				int mouseX, mouseY;
				SDL_GetMouseState(&mouseX, &mouseY);
				//_rects.push_back({ mouseX, mouseY, 50, 50 });
				break;
			default:
				break;
			}
		}
	}
}

void MainGame::Update()
{
	//std::cout << "Delta time: " << tick.GetTimeMilliseconds() << "ms\n";
	_gameObject->Update();
	//std::cout << "Position: " << _square.x << std::endl;
}

void MainGame::Draw()
{
	SDL_RenderClear(_renderer);
	SDL_SetRenderDrawColor(_renderer, 128, 128, 128, 255);
	SDL_RenderClear(_renderer);
	_gameObject->Render();

	SDL_RenderCopy(_renderer, _texture, nullptr, &_textDimensions);
	
	SDL_RenderPresent(_renderer);
}

void MainGame::InitializeSquares()
{
	/*std::cout << "Type the initial number of squares: ";
	std::cin >> _squaresNum;

	for (int i = 0; i < _squaresNum; ++i)
	{
		int randX = (rand() % _screenWidth) + 1;
		int randY = (rand() % _screenHeight) + 1;
		_rects.push_back({ randX, randY, 50, 50 });
	}*/
}
