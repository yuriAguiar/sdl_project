#pragma once

#include <SDL.h>

const int TARGET_FPS = 60;
const int FRAME_DELAY = 1000 / TARGET_FPS;

class Ticker
{
public:
	Ticker(Uint32 time = 0.0f)
		: _time(time)
	{
	}

	Uint32 GetTimeMilliseconds() const { return _time; };
	void CapFps();
	int CalculateLastFrameTime();

private:
	Uint32 _time;
	int _lastFrameTime = 0;
};
