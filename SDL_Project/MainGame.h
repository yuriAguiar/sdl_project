#pragma once

#include <SDL.h>
#include <glew.h>
#include <vector>
#include <SDL_ttf.h>
#include <string>

class Square;
class Ticker;
class GameObject;

enum class GameState
{
	PLAY, EXIT
};

class MainGame
{
public:
	MainGame();
	~MainGame();

	void Run();

private:
	void InitSystems();
	void GameLoop();
	void ProcessInput();
	void Update();
	void Draw();
	void InitializeSquares();

	SDL_Window* _window;
	SDL_Renderer* _renderer;
	GameState _gameState;

	// Square display
	//SDL_Rect _square;
	GameObject* _gameObject;
	int _screenWidth;
	int _screenHeight;
	int _squaresNum;

	// Text display
	SDL_Rect _textDimensions;
	TTF_Font* _font = nullptr;
	SDL_Texture* _texture = nullptr;
	std::string _text = "Text";
};
