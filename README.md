# SDL_Project

Visual Studio Config Instructions

- Make sure the Solution Configurarion is set to Debug and Solution Platform is set to x86

- Go to Project -> SDLProject properties. Set Configuration to All Configurations and Platform to Win32

- Go to Project -> SDLProject properties -> C/C++ -> Additional Include Directories. Add the following paths: "$(SolutionDir)Dependencies\SDL2\include", "$(SolutionDir)Dependencies\GLEW\include\GL", "$(SolutionDir)Dependencies\SDL2_ttf\include"

- Go to Project -> SDLProject properties -> Linker -> General -> Additional Library Directories. Add te following paths: "$(SolutionDir)Dependencies\SDL2_ttf\lib\x86", "$(SolutionDir)Dependencies\SDL2\lib\x86", "$(SolutionDir)Dependencies\GLEW\lib\Release\Win32"

- Go to Project -> SDLProject properties -> Linker -> Input -> Additional Dependencies. Add the following Libraries:
    SDL2_ttf.lib
    opengl32.lib
    glew32.lib
    glew32s.lib
    SDL2.lib
    SDL2main.lib
    kernel32.lib
    user32.lib
    gdi32.lib
    winspool.lib
    comdlg32.lib
    advapi32.lib
    shell32.lib
    ole32.lib
    oleaut32.lib
    uuid.lib
    odbc32.lib
    odbccp32.lib

- Done.